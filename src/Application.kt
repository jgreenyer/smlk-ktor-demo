package org.scenariotools.smlk

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.gson.*
import io.ktor.features.*
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import kotlinx.coroutines.launch

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    // Next exercise #1: Handle JSON-serialized parameters objects
    install(ContentNegotiation) {
        gson {
        }
    }

    // Next exercise #2: call other services using an HTTP client
    val httpClient = HttpClient(Apache) {
    }

    // create main object representing this service in the scenario program
    val advertisementService = AdvertisementService()

    // scenario program initialization and adding a simple example scenario
    val scenarioProgram = ScenarioProgram()
    scenarioProgram.environmentMessageTypes.add(AdvertisementService::currentVehicleLocation)
    scenarioProgram.activeGuaranteeScenarios.add(
        scenario(ANY sends AdvertisementService::currentVehicleLocation.symbolicEvent()){
            val sender = it.sender as Client
            val location = it.parameters[0] as Location
            request(sender.responseText("Hello Scenarios! Received location $location"))
        }
    )

    // setting up an EventNode for communicating with the scenario program
    val applicationEventNode = EventNode() // event node of this application
    scenarioProgram.eventNode.registerSender(applicationEventNode, UntypedSymbolicObjectEvent(advertisementService)) // register application event node as a sender/source of events where the receiver is advertisementService
    applicationEventNode.registerSender(scenarioProgram.eventNode, Client::responseText.symbolicEvent()) // register scenario program event node as a sender/source of events where the receives is a Client instance

    // run scenario program
    launch { scenarioProgram.run() }

    routing {
        get("/") {
            call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
        }

        get("/json/gson") {
            call.respond(mapOf("hello" to "world"))
        }

        // test this e.g. with calling http://localhost:8080/advertisementservice/currentVehicleLocation?longitude=34.0&latitude=45.0
        // you should see "Hello Scenarios! Received location Location(longitude=34.0, latitude=45.0)"
        get("/advertisementservice/currentVehicleLocation") {
            // retrieve parameter data
            val longitude = call.request.queryParameters["longitude"]
            val latitude = call.request.queryParameters["latitude"]

            // convert parameter data to objects as expected by the scenarios
            val location = if (longitude != null && latitude != null)
                Location(longitude.toDouble(),latitude.toDouble())
            else
                Location(0.0,0.0)

            val client = Client() // create new client object for each opened HTTP connection
            applicationEventNode.send(client sends advertisementService.currentVehicleLocation(location))

            // DO THIS ONLY WHEN YOU WANT TO SEND A PARTICULAR RESPONSE.
            // (Leave out if the scenarios do not request any response event, i.e., the request is just fire-forget)

            val responseEvent = waitForResponseEvent(client, applicationEventNode)

            call.respondText(responseEvent.parameters[0] as String, contentType = ContentType.Text.Plain)

        }
    }
}

suspend fun waitForResponseEvent(receiver : Any, applicationEventNode: EventNode) : ObjectEvent<*,*> {
    do {
        val responseEvent = applicationEventNode.receive() as ObjectEvent<*,*>
        if (responseEvent.receiver == receiver){ // seeing the response event corresponding to this call? (c.f. client object created above)
            return responseEvent
        }
    } while (true)
}


data class Location(val longitude: Double, val latitude: Double)
class Client(){
    fun responseText(text : String) = event(text){}
}
class AdvertisementService{
    fun currentVehicleLocation(location: Location) = event(location){}
}

